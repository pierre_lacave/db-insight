(function(){
  'use strict';
  
  var dashboardCtrl = function($state, WorkService, BackendsService, $location, $anchorScroll) {
    var that = this;

    this.works = [];
    this.addWork = _addWork;
    this.removeWork = _removeWork;
    this.getBackends = _getBackends;
    
    WorkService.getAll().then(function(res) {
      that.works = res.data.works;
    });    
    
    function _removeWork(index, workId) {
      WorkService.delete(workId).then(function() {
        that.works.splice(index, 1);
      }); 
    };
    
    function _addWork(backend) {
      this.works.push({backend:backend.id});
    };
    
    function _getBackends(cb) {
      BackendsService.getAll().then(function(resp) {
        that.backends = resp.data.backends;
        cb();
      });
    };
  };

  angular
  .module('dashboard', [
    'work'
  ])
  .config(['$stateProvider', function($stateProvider) {
    $stateProvider
    .state('in.dashboard', {
      url: '/dashboard',
      template: JST['modules/dashboard/dashboard.html'](),
      controller: 'DashboardCtrl',
      controllerAs: 'dashboard',
      authenticate: true
    });
  }])
  .controller('DashboardCtrl', [
  '$state',
  'WorkService',
  'BackendsService',
  '$location',
  '$anchorScroll',
  dashboardCtrl
  ]);

})();
