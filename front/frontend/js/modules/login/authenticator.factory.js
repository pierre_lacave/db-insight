(function(){
  'use strict';

  var authenticator = function($q, $log, $rootScope, localStorageService, LoginService, Session) {
    
    function _login(email, password) {
      var dfd = $q.defer();
      
      LoginService
      .login({username: email, password: password})
      .then(
        function(res) {
          Session.token = res.data.token;
          localStorageService.set('token', Session.token);
          $log.debug("authenticator.login.ok", email, password, Session );
          $rootScope.$broadcast('session:change', Session);
          dfd.resolve(Session);
        },
        function(res) {
          $log.debug("authenticator.login.error", res);
          dfd.reject( res );
        }
      ); 
      
      return dfd.promise;
    };
    
    function _logout() {
      var dfd = $q.defer();
      
      localStorageService.remove('token');
      Session.token = null;
      $log.debug("authenticator.logout.ok");
      $rootScope.$broadcast('session:change', Session);
      dfd.resolve(Session);
      
      return dfd.promise;
    };
   
    function _signup(email, password) {
      var dfd = $q.defer();
      
      LoginService
      .signup({email: email, password: password})
      .then(
        function(res) {
          $log.debug("authenticator.signup.ok", res, email);
          _login(email, password)
          .then(function(res) {
              dfd.resolve(res);
            });
        },
        function(res) {
          $log.debug("authenticator.signup.error", res);
          dfd.reject( res );
        }
      ); 
      
      return dfd.promise;
    };
   
    
    function _isLoggedIn() {
      return Session.token != null;
    };
    
    return {
      login: _login,
      logout: _logout,
      signup: _signup,
      isLoggedIn: _isLoggedIn
    }
  };

  angular
  .module('login')
  .factory('Authenticator', [
  '$q',
  '$log',
  '$rootScope',
  'localStorageService',
  'LoginService',
  'Session',
  authenticator
  ]);

})();
