require('angular-mocks');

describe("Login Controller Test Suite", function() {
  var $httpBackend;
  beforeEach(angular.mock.module('mustachedOctoNemesis'));
  beforeEach(angular.mock.module('login'));
  beforeEach(inject(function(_$httpBackend_) {
    $httpBackend = _$httpBackend_;
  }));
  
  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
    $httpBackend.resetExpectations();
  })
  
  it('Signup', inject(function($rootScope, $controller, Session) {
    var $scope = $rootScope.$new();
    var ctrl = $controller('LoginController', {$scope: $scope});
    
    
    $httpBackend.expectPOST('http://localhost:5000/user', {email:"pierre@lacave.me", password:"testtest"})
      .respond(200, {}, {});
    $httpBackend.expectPOST('http://localhost:5000/auth', {username:"pierre@lacave.me", password:"testtest"})
      .respond(200, {token:"thetoken"}, {});
    
    ctrl.email = "pierre@lacave.me"
    ctrl.password = "testtest";
    ctrl.userSignup();
    $httpBackend.flush();
    
    expect(Session.token).toBe("thetoken");
  }));
  
  it('Login', inject(function($rootScope, $controller, Session) {
    var $scope = $rootScope.$new();
    var ctrl = $controller('LoginController', {$scope: $scope});
    
    $httpBackend.expectPOST('http://localhost:5000/auth', {username:"pierre@lacave.me", password:"testtest"})
      .respond(200, {token:"thetoken"}, {});
    
    ctrl.email = "pierre@lacave.me"
    ctrl.password = "testtest";
    ctrl.userLogin();
    $httpBackend.flush();

    expect(Session.token).toBe("thetoken");
  }));
  
});