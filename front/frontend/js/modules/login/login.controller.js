(function(){
'use strict';

var loginController = function($state, $log, LoginService, Authenticator, Session) {
  var that = this;
  
  this.userLogin = _login;
  this.userSignup = _signup;
  
  function _login() {
    Authenticator.login(this.email, this.password)
      .then(
        function(res) {
          $state.go('in.dashboard');
      })
      .catch(function(res) {
          $log.debug('login: error', res);
      });
  };
  
  function _signup() {    
    Authenticator.signup(this.email, this.password)
    .then(
      function(res) {
        $state.go('in.dashboard');
      }, 
      function(res) {
        //$log.debug('login: error', res);
      });
  };
};

angular
  .module('login', [])
  .config(['$stateProvider', function($stateProvider) {
    $stateProvider
      .state('in.login', {
        url: '/login',
        template: JST['modules/login/login.html'](),
        controller: 'LoginController',
        controllerAs: 'login',
        authenticate: false
      })
      .state('in.signup', {
        url: '/signup',
        template: JST['modules/login/signup.html'](),
        controller: 'LoginController',
        controllerAs: 'signup',
        authenticate: false
      });
  }])
  .controller('LoginController', [
    '$state',
    '$log',
    'LoginService',
    'Authenticator',
    'Session',
    loginController
  ]);

})();
