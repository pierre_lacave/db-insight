require('angular-mocks');

describe("Login Service Test Suite", function() {
  var $httpBackend;
  beforeEach(angular.mock.module('mustachedOctoNemesis'));
  beforeEach(angular.mock.module('login'));
  beforeEach(inject(function(_$httpBackend_) {
    $httpBackend = _$httpBackend_;
  }));
  
  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
    $httpBackend.resetExpectations();
  })
  
  it('Login', inject(function(LoginService) {    
    $httpBackend.expectPOST('http://localhost:5000/auth', {})
      .respond(200, {}, {});

    LoginService.login({});
        
    $httpBackend.flush();
  }));
  it('Signup', inject(function(LoginService) {    
    $httpBackend.expectPOST('http://localhost:5000/user', {})
      .respond(200, {}, {});
          
    LoginService.signup({});
        
    $httpBackend.flush();
  })); 
  it('Me', inject(function(LoginService) {    
    $httpBackend.expectGET('http://localhost:5000/user')
      .respond(200, {}, {});

    LoginService.me();
        
    $httpBackend.flush();
  })); 
  
});