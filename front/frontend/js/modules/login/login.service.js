(function(){
'use strict';

var loginService = function($q, $http, ApiInfo) {

  this.login = function(data) {
    return $http.post(ApiInfo.url + '/auth', data);
  };
  this.signup = function(data) {
    return $http.post(ApiInfo.url + '/user', data);
  };
  this.me = function() {
    return $http.get(ApiInfo.url + '/user');
  };
};

angular
  .module('login')
  .service('LoginService', [
    '$q',
    '$http',
    'ApiInfo',
    loginService
  ]);
})();
