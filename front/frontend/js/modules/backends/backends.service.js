(function(){
  'use strict';

  var backendsService = function($q, $http, ApiInfo) {
    var TYPES = [
          {
            norm: "postgres",
            name: "PostgreSQL",
            icon: 'dbs-postgresql'
          }
        ];
        
    this.types = function() {
      return TYPES;
    }
    this.create = function(data) {
      return $http.post(ApiInfo.url + '/backend', data);
    };
    this.update = function(data) {
      return $http.put(ApiInfo.url + '/backend/' + data.id , data);
    };
    this.get = function(id) {
      return $http.get(ApiInfo.url + '/backend/'+ id );
    };
    this.getDefinition = function(id) {
      return $http.get(ApiInfo.url + '/backend/'+ id + '/definition', {cache:true});
    };
    this.getAll = function() {
      return $http.get(ApiInfo.url + '/backend');
    };
    this.delete = function(id) {
      return $http.delete(ApiInfo.url + '/backend/'+ id );
    };
  };

  angular
  .module('backends')
  .service('BackendsService', [
  '$q',
  '$http',
  'ApiInfo',
  backendsService
  ]);

})();
