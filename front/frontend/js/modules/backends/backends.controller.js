(function(){
  'use strict';

  var backendsCtrl = function($rootScope, $state, $log, BackendsService) {
    var that = this;
    
    this.goCreate = _goCreate;
    this.editBackend = _goEdit;
    this.deleteBackend = _delete;
    this.icon = _getIcon;
    
    _getBackends();
    
    function _goCreate() {
      $state.go('in.backends.create');
    };
    function _getBackends() {
      BackendsService.getAll()
      .then(
        function(res) {
          that.backends = res.data.backends;
          $log.debug('backend.getBackends.ok', res.data);
        },
        function(err) {
         $log.error('backend.getBackends.error', err); 
        });
    };
    function _getIcon(backend) {
      return BackendsService.types().filter(function ( obj ) {
        return obj.norm === backend.type;
      })[0].icon;
    };
    function _goEdit(backend) {
      $state.go('in.backends.edit', {'id' : backend.id});
    };
    function _delete(backend) {
      $log.debug('backend.delete.' + backend.id)
      BackendsService.delete(backend.id)
      .then(function() {
        _getBackends();
      })
      .catch(function(err) {
         $log.error('backend.delete.err', err); 
      });
    };
  };

  var backendsFormCtrl = function($rootScope, $state, $stateParams, $log, BackendsService) {
    var that = this;
    
    this.types = BackendsService.types();
    this.save = _save;
    this.data = null;
    
    if ($stateParams.id) {
      BackendsService.get($stateParams.id)
        .then(
          function(res) {
            $log.debug('backend.form.getOne.ok', res.data);
            that.data = res.data;
          },
          function(err) {
            $log.debug('backend.form.getOne.error', err);
          });
    }

    function _save() {
      if ($stateParams.id) {
        BackendsService.update(that.data)
          .then(
            function(res) {
              $log.debug('backend.form.save.update.ok', res.data);
              $state.go("in.backends.list");
            },
            function(err) {
              $log.debug('backend.form.save.update.err', err);
            });
      } else {
        BackendsService.create(that.data)
          .then(
            function(res) {
              $log.debug('backend.form.save.create.ok', res.data);
              $state.go("in.backends.list");
            },
            function(err) {
              $log.debug('backend.form.save.update.err', err);
            });
      }
    };
  };

  angular
  .module('backends', [])
  .config(['$stateProvider', function($stateProvider) {
    $stateProvider
    .state('in.backends', {
      url: '/backends',
      template: JST['modules/backends/backends.html'](),
    })
    .state('in.backends.list', {
      url: '/backends/list',
      template: JST['modules/backends/list.html'](),
      controller: 'BackendsCtrl',
      controllerAs: 'backends',
      authenticate: true
    })
    .state('in.backends.create', {
      url: '/backends/create',
      template: JST['modules/backends/form.html'](),
      controller: 'BackendsFormCtrl',
      controllerAs: 'backendsForm',
      authenticate: true
    })
    .state('in.backends.edit', {
      url: '/backends/:id/edit',
      template: JST['modules/backends/form.html'](),
      controller: 'BackendsFormCtrl',
      controllerAs: 'backendsForm',
      authenticate: true
    });
  }])
  .controller('BackendsCtrl', [
  '$rootScope',
  '$state',
  '$log',
  'BackendsService',
  backendsCtrl
  ])
  .controller('BackendsFormCtrl', [
  '$rootScope',
  '$state',
  '$stateParams',
  '$log',
  'BackendsService',
  backendsFormCtrl
  ]);

})();
