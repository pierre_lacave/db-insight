require('angular-mocks');

describe("Backend Service Test Suite", function() {  
  var $httpBackend;
  beforeEach(angular.mock.module('mustachedOctoNemesis'));
  beforeEach(angular.mock.module('backends'));


  beforeEach(inject(function(_$httpBackend_) {
    $httpBackend = _$httpBackend_;
  }));
  
  afterEach(function() {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
    $httpBackend.resetExpectations();
  })
  
  it('Create', inject(function(BackendsService) {    
    $httpBackend.expectPOST('http://localhost:5000/backend', {})
      .respond(200, {}, {});
    BackendsService.create({});
    $httpBackend.flush();
  }));
  it('Update', inject(function(BackendsService) {    
    $httpBackend.expectPUT('http://localhost:5000/backend/1', {id: 1})
      .respond(200, {}, {});
    BackendsService.update({id:1});
    $httpBackend.flush();
  })); 
  it('Get', inject(function(BackendsService) {    
    $httpBackend.expectGET('http://localhost:5000/backend/1')
      .respond(200, {}, {});
    BackendsService.get(1);
    $httpBackend.flush();
  })); 
  it('GetDefinition', inject(function(BackendsService) {    
    $httpBackend.expectGET('http://localhost:5000/backend/1/definition')
      .respond(200, {}, {});
    BackendsService.getDefinition(1);
    $httpBackend.flush();
  }));
  it('GetAll', inject(function(BackendsService) {    
    $httpBackend.expectGET('http://localhost:5000/backend')
      .respond(200, {}, {});
    BackendsService.getAll();
    $httpBackend.flush();
  }));   
  it('Delete', inject(function(BackendsService) {    
    $httpBackend.expectDELETE('http://localhost:5000/backend/1')
      .respond(200, {}, {});
    BackendsService.delete(1);
    $httpBackend.flush();
  }));
});