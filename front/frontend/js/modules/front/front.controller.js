(function(){
  'use strict';

  var frontCtrl = function($state, $rootScope) {
  };

  angular
  .module('front', [])
  .config(['$stateProvider', function($stateProvider) {
    $stateProvider
    .state('in.front', {
      url: '/',
      template: JST['modules/front/front.html'](),
      controller: 'FrontCtrl',
      controllerAs: 'front',
      authenticate: false
    });
  }])
  .controller('FrontCtrl', [
  '$state',
  '$rootScope',
  frontCtrl
  ]);

})();
