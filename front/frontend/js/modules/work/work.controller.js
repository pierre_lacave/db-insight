(function(){
  'use strict';

  var d3 = require('d3');
  var nv = require('nvd3');

  var workCtrl = function($scope, $log,  $q, BackendsService, WorkService, WorkDataService) {
    var that = this;
    
    this.item = $scope.workitem;

    initialise();

    that.dataOrig1 = [{
      key: "Cumulative Return",
      values: [
      ]
    }];

    that.data = that.dataOrig1;
 
    function initialise() {
      if (typeof(that.item.id) == "undefined") {
        WorkService.create(that.item)
        .then(function(data) {
          $log.debug('workctrlr.init.ok', that.item);
          that.item = data.data;
          WorkService.getSchema(that.item)
          .then(function(data) {
            that.schema = data;
          });
        })
        .catch(function(err) {
          $log.error('workctrlr.init.error', err);
        });
      } else {  
        WorkService.getSchema(that.item).then(function(data) {
          that.schema = data;
          WorkDataService.get(that.item.id)
          .then(function(data) {
            console.debug(data.data.data);
            that.data[0].values = data.data.data;
          })
        });
      }  
    }


    $scope.$watch('work.item.config', function(newValue, oldValue) {
      if (newValue !== oldValue && newValue != null) {
        $log.debug("watch work.item.config", newValue, oldValue)
        if (that.item.config.measure && that.item.config.dimension) {
          var workConfig = {
            id: that.item.id,
            config: that.item.config
          };
          WorkService.update(workConfig).then(function() {
            WorkDataService.get(that.item.id)
            .then(function(data) {
              console.debug(data);
              that.data[0].values = data.data.data;
            });
          });
        } 
      }
    }, true);
    
    that.options = {
      chart: {
          type: 'discreteBarChart',
          height: 300,
          margin : {
              top: 20,
              right: 20,
              bottom: 40,
              left: 60
          },
          x: function(d){ return d.label; },
          y: function(d){ return d.count; },
          showValues: true,
          valueFormat: function(d){
              return d3.format()(d);
          },
          autorefresh: true,
          transitionDuration: 500,
          xAxis: {
              axisLabel: "dimension"
          },
          yAxis: {
              axisLabel: "measure"
          }
      }
    };
  };

  angular
  .module('work', [])
  .controller('WorkCtrl', [
  '$scope',
  '$log',
  '$q',
  'BackendsService',
  'WorkService',
  'WorkDataService',
   workCtrl
  ]);

})();
