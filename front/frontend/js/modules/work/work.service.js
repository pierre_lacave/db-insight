(function(){
  'use strict';
  
  require('../backends/backends.service');

  var workService = function($q, $http, ApiInfo, BackendsService) {

    this.create = function(data) {
      return $http.post(ApiInfo.url + '/work', data);
    };
    this.update = function(data) {
      return $http.put(ApiInfo.url + '/work/' + data.id , data);
    };
    this.get = function(id) {
      return $http.get(ApiInfo.url + '/work/'+ id );
    };
    this.getAll = function() {
      return $http.get(ApiInfo.url + '/work');
    };
    this.delete = function(id) {
      return $http.delete(ApiInfo.url + '/work/'+ id );
    };
    
    this.getSchema = function(data) {
        var dfd = $q.defer();
        BackendsService.getDefinition(data.backend_id)
        .then(function(data) {
          dfd.resolve(data.data.schema);
        })
        .catch(function(err) {
          dfd.reject(err);
        });
        
        return dfd.promise;
    }
  };

  angular
  .module('work')
  .service('WorkService', [
  '$q',
  '$http',
  'ApiInfo',
  'BackendsService',
  workService
  ]);

})();
