(function(){
  'use strict';

  var workDataService = function($q, $http, ApiInfo) {
    this.get = function(id) {
      return $http.get(ApiInfo.url + '/work/'+ id + '/data' );
    };
  };

  angular
  .module('work')
  .service('WorkDataService', [
  '$q',
  '$http',
  'ApiInfo',
  workDataService
  ]);

})();
