(function(){
  'use strict';

  angular
  .module('constants', [])
  .constant('ApiInfo', {
    url: "http://localhost:5000"
  })
  .value('Session', {
    token: null
  });

})();
