(function() {
  "use strict";

  var globalCtrl = function($state, $log, $rootScope, Authenticator) {
    var that = this;
     
    that.loggedin = Authenticator.isLoggedIn();
    that.logout = _logout;
     
    $rootScope.$on('session:change', function(evt, dat) {
      $log.debug("globalCtr.watch.session:change", evt, dat);
      that.loggedin = Authenticator.isLoggedIn();
    });
     
    function _logout() {
      Authenticator.logout().then(function() {
        $state.go("in.front");    
      });       
    }  
  };

  angular
  .module('layouts', [])
  .config(['$stateProvider', function($stateProvider) {
    $stateProvider
    .state('in',{
      template: JST['layouts/in.html'](),
      controller: 'GlobalCtrl',
      controllerAs: 'global'
    })
    .state('out',{
      template: JST['layouts/out.html']()
    });
  }])
  .controller('GlobalCtrl', [
    '$state',
    '$log',
    '$rootScope',
    'Authenticator',
    globalCtrl
  ]);
}());
