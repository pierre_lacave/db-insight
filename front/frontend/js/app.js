(function(){
'use strict';
require('angular');
require('angular-ui-router');
require('angular-gravatar');
require('angular-material');
require('angular-material-icons');
require('angular-local-storage');
require('angular-nvd3');

require('./modules/login');
require('./modules/front');
require('./modules/backends');
require('./modules/dashboard');
require('./layouts');
require('./constants');

window._  = require('underscore');

angular
  .module('mustachedOctoNemesis', [
    'ui.router',
    'ui.gravatar',
    'ngMaterial',
    'ngMdIcons',
    'LocalStorageModule',
    'nvd3',
    'login',
    'front',
    'backends',
    'dashboard',
    'layouts',
    'constants',
  ])
  .factory('AuthInterceptor', ['$q', '$injector', function($q, $injector) {
    var LocalService = $injector.get('localStorageService');
    var Session = $injector.get('Session');
    
    return {
      request: function(config) {
        var token;
        if (LocalService.get('token')) {
          config.headers.Authorization = 'JWT ' +  LocalService.get('token');
        }
        return config;
      },
      responseError: function(response) {
        var Authenticator = $injector.get('Authenticator');
        if (response.status === 401 || response.status === 403) {
          var mdToast = $injector.get('$mdToast');
          mdToast.show(
            mdToast.simple()
              .content(response.data)
              .position("top left")
              .hideDelay(3000)
          );
          Authenticator.logout();
          $injector.get('$state').go('in.login');
            //toastr.error(response.data.message, 'Error');
        }
        return $q.reject(response);
      }
    };
  }])
  .config([
    '$urlRouterProvider',
    'localStorageServiceProvider',
    '$httpProvider',
    'gravatarServiceProvider',
    '$mdThemingProvider',
    '$provide',
    function($urlRouterProvider, localStorageServiceProvider, $httpProvider, gravatarServiceProvider, $mdThemingProvider, $provide) {
      $urlRouterProvider.otherwise('/');
      localStorageServiceProvider.setPrefix('mustached');
      $httpProvider.interceptors.push('AuthInterceptor');
      gravatarServiceProvider.defaults = {
        size     : 100,
        "default": 'retro'  // Mystery man as default for missing avatars
      };
      gravatarServiceProvider.secure = true;
      $mdThemingProvider.theme('default')
        .primaryPalette('teal')
        .accentPalette('amber')
        .warnPalette('pink');

        //Available palettes: red, pink, purple, deep-purple, indigo, blue, light-blue, cyan, teal, green, light-green, lime, yellow, amber, orange, deep-orange, brown, grey, blue-grey
        //theme.primaryPalette, theme.accentPalette, theme.warnPalette, theme.backgroundPalette
        
      
      $provide.decorator('$templateCache', ['$delegate', '$sniffer', function($delegate, $sniffer) {
        var originalGet = $delegate.get;
        $delegate.get = function(key) {
          var value;
          value = originalGet(key);
          if (!value) {
            // JST is where my partials and other templates are stored
            // If not already found in the cache, look there...
            value = JST[key]();
            if (value) {
              $delegate.put(key, value);
            }
          }
          return value;
        };
        return $delegate;
      }]);
      
      $provide.decorator('$log', ['$delegate', '$sniffer', '$injector', function($delegate, $sniffer, $injector) {
        var _error = $delegate.error;
        $delegate.error = function(msg) {
          var $mdToast = $injector.get('$mdToast');
          _error.apply(this, arguments);
          $mdToast.show({
                template: '<md-toast class="md-toast error">' + msg + '</md-toast>',
                hideDelay: 10000,
                position: 'top left'
            });
        };
        
        var _log = $delegate.log;
        $delegate.log = function(msg) {
          var $mdToast = $injector.get('$mdToast');
          _log.apply(this, arguments);
          $mdToast.show({
                template: '<md-toast class="md-toast log">' + msg + '</md-toast>',
                hideDelay: 5000,
                position: 'bottom left'
            });
        };
  
        var _debug = $delegate.debug;
        $delegate.debug = function(msg, args, arg2) {
          var $mdToast = $injector.get('$mdToast');
          _debug.apply(this, arguments);
          $mdToast.show({
                template: '<md-toast class="md-toast">' + msg + '</md-toast>',
                hideDelay: 3000,
                position: 'bottom right'
            });
        };
        $delegate.debug.logs = []
        
		    return $delegate;
  	  }]);
    }
  ])
  .run([
    '$rootScope',
    '$state',
    '$http',
    '$log',
    '$location',
    '$anchorScroll',
    'Session',
    'localStorageService',
    function ($rootScope, $state, $http, $log, $location, $anchorScroll, Session, localStorageService) {
      Session.token = localStorageService.get('token');     
      $rootScope.$on("$stateChangeStart", function(e, toState, toParams, fromState, fromParams) {
        if (toState.authenticate && !Session.token) {
          $state.transitionTo("in.front");
          e.preventDefault();
        }
      });
    }
  ]);
  
})();