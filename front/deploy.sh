#!/bin/bash

if [[ $1 == CODESHIP ]]; then
    echo "CODESHIP"
else
    echo "Thats not what you want"
    exit
fi

#build it
gulp prod

#change to whichever directory this lives in
rm -rf deploy/www
mv www deploy
cd deploy

#create new git repository and add everything
git init
git add .
git commit -m"pushy"
git remote add dokku-deploy dokku@zoidberg.xyz:sight-evens-linli


#push back to heroku, open web browser, and remove git repository
git push -f dokku-deploy master
rm -fr .git

#go back to wherever we started.
cd -