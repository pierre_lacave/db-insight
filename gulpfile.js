var gulp = require('gulp');
var shell = require('gulp-shell');

gulp.task('flask-dev', shell.task(['. env/bin/activate && python backend/server.py']));
gulp.task('flask-prod', shell.task(['. env/bin/activate && gunicorn -w 4 --pythonpath backend server:app --log-file -']));

gulp.task('dev', ['flask-dev']);
gulp.task('prod', ['flask-prod']);
