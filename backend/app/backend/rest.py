from flask import jsonify
from flask.ext import restful
from flask.ext.restful import reqparse
from flask_jwt import jwt_required, current_user
from app.backend.model import Backend
from app.backend.schema import backends_schema, backend_schema
from app.extensions import db
from app.engines.postgres import Postgres

post_parser = reqparse.RequestParser()
post_parser.add_argument('name', required=True, type=str)
post_parser.add_argument('type', required=True, type=str)
post_parser.add_argument('host', required=True, type=str)

class BackendRestList(restful.Resource):
    @jwt_required()
    def get(self):
        backends = Backend.query.filter_by(user_id=current_user.id)
        # Serialize the queryset
        result = backends_schema.dump(backends)
        return jsonify({'backends': result.data})

    @jwt_required()
    def post(self):
        args = post_parser.parse_args()
        p = Postgres(args.host)
        try:
            p.isConnectionValid()
        except Exception, err:
            return str(err), 406
        else:
            newbackend = Backend(args.type, args.host, args.name, current_user.id)
            db.session.add(newbackend)
            db.session.commit()
            result = backend_schema.dump(newbackend)
            return result.data, 201


class BackendRest(restful.Resource):
    @jwt_required()
    def get(self, backend_id):
        backend = Backend.query\
            .filter_by(id=backend_id, user_id=current_user.id)\
            .one()
        result = backend_schema.dump(backend)
        return result.data, 200

    @jwt_required()
    def put(self, backend_id):
        args = post_parser.parse_args()
        p = Postgres(args.host)
        try:
            p.isConnectionValid()
        except Exception, err:
            return str(err), 406
        else:
            Backend.query\
                .filter_by(id=backend_id, user_id=current_user.id)\
                .update(dict(name=args.name, host=args.host, btype=args.type))
            db.session.commit()
            backend = Backend.query\
                .filter_by(id=backend_id)\
                .first()
            result = backend_schema.dump(backend)
            return result.data, 201

    @jwt_required()
    def delete(self, backend_id):
        backend = Backend.query\
            .filter_by(id=backend_id, user_id=current_user.id)\
            .first()
        db.session.delete(backend)
        db.session.commit()
        return '', 204


class BackendDefinitionRest(restful.Resource):
    @jwt_required()
    def get(self, backend_id):

        backend = Backend.query\
            .filter_by(id=backend_id)\
            .first()
        p = Postgres(backend.host)
        return jsonify(schema=p.getDefinition())
