from marshmallow import Schema, fields, ValidationError, pre_load

class BackendSchema(Schema):
    id = fields.Int(dump_only=True)
    type = fields.Str(attribute='btype')
    host = fields.Str()
    name = fields.Str()

backend_schema = BackendSchema()
backends_schema = BackendSchema(many=True)
