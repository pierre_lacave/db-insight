from app.extensions import db
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy.types import Enum

backends_enum = ('postgres','mongodb')

class Backend(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    btype = db.Column(Enum(*backends_enum, name='backend_type'))
    host = db.Column(db.String(1024))
    name = db.Column(db.String(1024))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __init__(self, btype, host, name, user_id):
        self.btype = btype
        self.host = host
        self.name = name
        self.user_id = user_id

    def __repr__(self):
        return '<Backend %r>' % self.btype
