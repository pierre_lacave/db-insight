# -*- coding: utf-8 -*-
"""
    application.py
    ~~~~~~~~~~~

    DBInsight Application configuration
"""
from flask import Flask, Response, request, g, jsonify, redirect, url_for, flash
from app.config import DefaultConfig
from app.extensions import db, restful, jwt, cors

__all__ = ["create_app"]
DEFAULT_APP_NAME = "insight"


def create_app(config=None, app_name=None, modules=None):
    if app_name is None:
        app_name = DEFAULT_APP_NAME

    app = Flask(app_name)
    api = restful.Api()

    configure_app(app, config)
    configure_auth_handler(app)
    configure_api(app, api)
    configure_extensions(app, api)
    configure_api_doc(app)

    return app

def configure_app(app, config):
    app.config.from_object(DefaultConfig())

    if config is not None:
        app.config.from_object(config)

    app.config.from_envvar('APP_CONFIG', silent=True)


def configure_api(app, api):
    from app.backend.rest import BackendRestList, BackendRest, BackendDefinitionRest
    from app.work.rest import WorkRestList, WorkDataRest, WorkRest
    from app.user.rest import UserRest

    api.add_resource(BackendRestList, '/backend')
    api.add_resource(BackendRest, '/backend/<backend_id>')
    api.add_resource(BackendDefinitionRest, '/backend/<backend_id>/definition')
    api.add_resource(WorkRestList, '/work')
    api.add_resource(WorkRest, '/work/<work_id>')
    api.add_resource(WorkDataRest, '/work/<work_id>/data')
    api.add_resource(UserRest, '/user')


def configure_auth_handler(app):
    from app.user.model import User

    @jwt.authentication_handler
    def authenticate(username, password):
        attempted_user = User.query.filter_by(email=username).first()
        if attempted_user and attempted_user.check_password(password):
            return attempted_user

    @jwt.user_handler
    def load_user(payload):
        attempted_user = User.query.filter_by(id=payload['user_id']).first()
        if attempted_user is not None:
            return attempted_user


def configure_extensions(app, api):
    """
    configure extension with the app
    :param app: the created app to configure
    :param api: the created api to link to the app
    :return:
    """
    db.init_app(app)
    api.init_app(app)
    jwt.init_app(app)
    cors.init_app(app, max_age=3600)

def configure_api_doc(app):
    import flask_jwt
    from flask import jsonify
    from flask_swagger import swagger
    from app.doc import UserRestDoc, LoginRestDoc
    from app.doc import BackendRest
    import app.user.rest as user
    import app.backend.rest as backend

    flask_jwt.JWTAuthView.post.__func__.__doc__ = LoginRestDoc.post
    user.UserRest.post.__func__.__doc__ = UserRestDoc.post
    user.UserRest.get.__func__.__doc__ = UserRestDoc.getOne

    backend.BackendRestList.post.__func__.__doc__ = BackendRest.post
    backend.BackendRestList.get.__func__.__doc__ = BackendRest.getAll
    backend.BackendRest.get.__func__.__doc__ = BackendRest.getOne

    @app.route("/spec.json")
    def spec():
        swag = swagger(app)
        swag['info']['version'] = "0.1"
        swag['info']['title'] = "Insights API"
        return jsonify(swag)

    # Routes
    @app.route('/doc')
    def root():
      return app.send_static_file('index.html')

    @app.route("/doc/<path:path>")
    def static_files(path):
        return app.send_static_file(path)


if __name__ == '__main__':
    import argparse

    app = create_app()
    parser = argparse.ArgumentParser(description='Development Server Help')
    parser.add_argument("-d", "--debug", action="store_true", dest="debug_mode",
                        help="run in debug mode (for use with PyCharm)", default=False)
    parser.add_argument("-p", "--port", dest="port",
                        help="port of server (default:%(default)s)", type=int, default=5000)

    cmd_args = parser.parse_args()
    app_options = {"port": cmd_args.port}

    if cmd_args.debug_mode:
        app_options["debug"] = True
        app_options["use_debugger"] = False
        app_options["use_reloader"] = False

    app.run(**app_options)

# def configure_errorhandlers(app):

#     if app.testing:
#         return

#     @app.errorhandler(404)
#     def page_not_found(error):
#         if request.is_xhr:
#             return jsonify(error=_('Sorry, page not found'))
#         return render_template("errors/404.html", error=error)

#     @app.errorhandler(403)
#     def forbidden(error):
#         if request.is_xhr:
#             return jsonify(error=_('Sorry, not allowed'))
#         return render_template("errors/403.html", error=error)

#     @app.errorhandler(500)
#     def server_error(error):
#         if request.is_xhr:
#             return jsonify(error=_('Sorry, an error has occurred'))
#         return render_template("errors/500.html", error=error)

#     @app.errorhandler(401)
#     def unauthorized(error):
#         if request.is_xhr:
#             return jsonfiy(error=_("Login required"))
#         flash(_("Please login to see this page"), "error")
#         return redirect(url_for("account.login", next=request.path))


# def configure_logging(app):
#     if app.debug or app.testing:
#         return

#     mail_handler = \
#         SMTPHandler(app.config['MAIL_SERVER'],
#                     'error@lacave.me',
#                     app.config['ADMINS'], 
#                     'application error',
#                     (
#                         app.config['MAIL_USERNAME'],
#                         app.config['MAIL_PASSWORD'],
#                     ))

#     mail_handler.setLevel(logging.ERROR)
#     app.logger.addHandler(mail_handler)

#     formatter = logging.Formatter(
#         '%(asctime)s %(levelname)s: %(message)s '
#         '[in %(pathname)s:%(lineno)d]')

#     debug_log = os.path.join(app.root_path, 
#                              app.config['DEBUG_LOG'])

#     debug_file_handler = \
#         RotatingFileHandler(debug_log,
#                             maxBytes=100000,
#                             backupCount=10)

#     debug_file_handler.setLevel(logging.DEBUG)
#     debug_file_handler.setFormatter(formatter)
#     app.logger.addHandler(debug_file_handler)

#     error_log = os.path.join(app.root_path, 
#                              app.config['ERROR_LOG'])

#     error_file_handler = \
#         RotatingFileHandler(error_log,
#                             maxBytes=100000,
#                             backupCount=10)

#     error_file_handler.setLevel(logging.ERROR)
#     error_file_handler.setFormatter(formatter)
#     app.logger.addHandler(error_file_handler)
