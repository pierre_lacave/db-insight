from flask import jsonify
from flask.ext import restful
from flask.ext.restful import reqparse
from flask_jwt import jwt_required, current_user
from app.work.model import Work
from app.backend.model import Backend
from app.work.schema import work_schema, works_schema
from app.engines.postgres import Postgres
from app.extensions import db

import json

post_create_parser = reqparse.RequestParser()
post_create_parser.add_argument('backend', required=True, type=int)

post_update_parser = reqparse.RequestParser()
post_update_parser.add_argument('config', required=True, type=dict)


class WorkRestList(restful.Resource):
    @jwt_required()
    def post(self):
        args = post_create_parser.parse_args()
        newWork = Work(current_user.id, args.backend)
        db.session.add(newWork)
        db.session.commit()
        result = work_schema.dump(newWork)
        return result.data, 201

    @jwt_required()
    def get(self):
        works = Work.query.filter_by(user_id=current_user.id)
        result = works_schema.dump(works)
        return jsonify({'works': result.data})


class WorkRest(restful.Resource):
    @jwt_required()
    def put(self, work_id):
        args = post_update_parser.parse_args()
        Work.query\
            .filter_by(id=work_id, user_id=current_user.id)\
            .update(dict(config=args.config))
        db.session.commit()
        work = Work.query\
            .filter_by(id=work_id)\
            .first()
        result = work_schema.dump(work)
        return result.data, 201     

    @jwt_required()
    def delete(self, work_id):
        work = Work.query\
            .filter_by(id=work_id, user_id=current_user.id)\
            .first()
        db.session.delete(work)
        db.session.commit()
        return '', 204


class WorkDataRest(restful.Resource):
    @jwt_required()
    def get(self, work_id):
        work = Work.query\
            .filter_by(id=work_id, user_id=current_user.id)\
            .first()
    
        backend = Backend.query\
            .filter_by(id=work.backend_id)\
            .first()
            
        p = Postgres(backend.host)
        result = p.getData(work.config['measure'], work.config['dimension'])
        return jsonify(data=result)
