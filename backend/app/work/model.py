from app.extensions import db
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy.types import Enum
from sqlalchemy.dialects.postgresql import JSON
backends_enum = ('postgres','mongodb')

class Work(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete="CASCADE"))
    backend_id = db.Column(db.Integer, db.ForeignKey('backend.id', ondelete="CASCADE"))
    config = db.Column(JSON)

    def __init__(self, user_id, backed_id):
        self.user_id = user_id
        self.backend_id = backed_id

    def __repr__(self):
        return '<Work %r>' % self.id

    def to_json(self):
        return {
            'id': self.id
        }
