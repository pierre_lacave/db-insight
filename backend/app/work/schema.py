# -*- coding: utf-8 -*-
from marshmallow import Schema, fields, ValidationError, pre_load


class DimensionSchema(Schema):
    name = fields.Str()
    type = fields.Str()


class ConfigSchema(Schema):
    measure = fields.Str()
    dimension = fields.Nested(DimensionSchema)


class WorkSchema(Schema):
    id = fields.Int(dump_only=True)
    backend_id = fields.Int()
    config = fields.Nested(ConfigSchema)

work_schema = WorkSchema()
works_schema = WorkSchema(many=True)
