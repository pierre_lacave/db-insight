from sqlalchemy import create_engine
from sqlalchemy.engine import reflection
from sqlalchemy import func
from sqlalchemy.sql import text
from sqlalchemy.orm import sessionmaker
from sqlalchemy.schema import Table, MetaData
from sqlalchemy.sql import table, column, select
import traceback

class Postgres(object):
    def __init__(self, url):
        # 'mysql://scott:tiger@localhost/test'
        self.engine = create_engine(url)

    def isConnectionValid(self):

        insp = reflection.Inspector.from_engine(self.engine)
        #print insp.get_table_names()
        return True


    def getDefinition(self):
        res = {}
        insp = reflection.Inspector.from_engine(self.engine)
        for table in insp.get_table_names():
            t = res[table] = []
            for column in insp.get_columns(table):
                c = {
                  'name' : str(column['name']),
                  'type': str(column['type'])
                }
                t.append(c)
        return res
        
    def getData(self, measure, dimension):
        connection = self.engine.connect()
        meta = MetaData()
        measure = Table(measure, meta, autoload=True, autoload_with=self.engine)
        statement = select([
                    measure.c[dimension['name']].label("label"),
                    func.count(measure.c[dimension['name']]).label("count")
            ]).group_by(measure.c[dimension['name']])
            
            # .where(orders.c.region.in_(
            #     select([top_regions.c.region])
            # ))
        
        result = connection.execute(statement).fetchall()
        connection.close()
        return [dict(r) for r in result]
        

        #Session = sessionmaker(bind=self.engine)
        #session = Session()
        #return session.query(measure.dimen, func.count(dimension)).select_from(measure).group_by(dimension).all()
        # return session.query(func.count(),Table.column1, Table.column2).group_by(Table.column1, Table.column2).all()
        
        # # res = connection.execute(
        # #     text("SELECT :dimension, count(dimension) FROM :measure GROUP BY :dimension"),
        # #     dimension=dimension,
        # #     measure=measure
        # # )
        # # connection.close()
        # return res


        #connection = self.engine.connect()
        # result = connection.execute("select username from users")
        # for row in result:
        #     print "username:", row['username']
        # connection.close()
