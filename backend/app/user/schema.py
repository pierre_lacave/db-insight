from marshmallow import Schema, fields, ValidationError, pre_load

class UserSchema(Schema):
    id = fields.Int(dump_only=True)
    email = fields.Str()

user_schema = UserSchema()
