from flask.ext import restful
from flask.ext.restful import reqparse, abort
from flask_jwt import jwt_required, current_user
from app.user.model import User
from app.user.schema import user_schema
from app.extensions import db


post_parser = reqparse.RequestParser()
post_parser.add_argument('email', required=True, type=str)
post_parser.add_argument('password', required=True, type=str)


class UserRest(restful.Resource):
    @jwt_required()
    def get(self):
        result = user_schema.dump(current_user)
        return result.data, 200

    def post(self):
        args = post_parser.parse_args()
        existing_user = User.query\
            .filter_by(email=args.email)\
            .first()
        if existing_user is not None:
            abort(409, message="Email already registered")
        newuser = User(args.email, args.password)
        db.session.add(newuser)
        db.session.commit()
        result = user_schema.dump(newuser)
        return result.data, 201
