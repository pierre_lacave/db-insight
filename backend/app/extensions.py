
from flask.ext.sqlalchemy import SQLAlchemy
from flask_jwt import JWT
from flask.ext import restful
from flask.ext.cors import CORS


__all__ = ['db', 'api', 'jwt', 'cors']

db = SQLAlchemy()
restful = restful
jwt = JWT()
cors = CORS()
