# -*- coding: utf-8 -*-
"""
    config.py
    ~~~~~~~~~~~

    Default configuration

"""
import os

class DefaultConfig(object):
    """
    Default configuration
    """

    DEBUG = True
    SECRET_KEY = "ins!ghtful42$/&&!"

    # keys for localhost. Change as appropriate.

    RECAPTCHA_PUBLIC_KEY = '6LeYIbsSAAAAACRPIllxA7wvXjIE411PfdB2gt2J'
    RECAPTCHA_PRIVATE_KEY = '6LeYIbsSAAAAAJezaIq3Ft_hSTo0YtyeFG-JgRtu'

    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', 'postgresql://pierrelacave@localhost/pierrelacave')

    SQLALCHEMY_ECHO = False

    MAIL_DEBUG = DEBUG

    ADMINS = ()

    DEFAULT_MAIL_SENDER = "support@lacave.me"

    ACCEPT_LANGUAGES = ['en']

    DEBUG_LOG = 'logs/debug.log'
    ERROR_LOG = 'logs/error.log'

    CACHE_TYPE = "simple"
    CACHE_DEFAULT_TIMEOUT = 300


class TestConfig(object):

    TESTING = True
    CSRF_ENABLED = False
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', 'postgresql://pierrelacave@localhost/test')
    SQLALCHEMY_ECHO = False



