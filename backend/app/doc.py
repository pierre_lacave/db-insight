



class LoginRestDoc:
    post = """
            Login
            ---
            tags:
                - users
            parameters:
                - in: body
                  name: body
                  schema:
                    id: User
                    required:
                        - username
                        - password
                    properties:
                        username:
                            type: string
                            description: email for user
                        password:
                            type: string
                            description: password for user
            responses:
                201:
                    description: User created
            """

class UserRestDoc:
    post = """
            Create a new user
            ---
            tags:
                - users
            parameters:
                - in: body
                  name: body
                  schema:
                    id: User
                    required:
                        - email
                        - password
                    properties:
                        email:
                            type: string
                            description: email for user
                        password:
                            type: string
                            description: password for user
            responses:
                201:
                    description: User created
            """

    getOne = """
            Get current user
            ---
            tags:
                - users
            responses:
              201:
                description: User created
            """

class BackendRest:
    post = """
            Create a new backend
            ---
            tags:
                - backend
            parameters:
                - in: body
                  name: body
                  schema:
                    id: Backend
                    required:
                        - name
                        - host
                        - type
                    properties:
                        name:
                            type: string
                            description: name for backend
                        host:
                            type: string
                            description: address of database
                        type:
                            type: string
                            description: type of backend  (postgres)
            responses:
                201:
                    description: Backend created
            """

    getAll = """
            Get all Backends
            ---
            tags:
                - backend
            responses:
              201:
                description: backends
            """

    getOne = """
            Get a Backend
            ---
            tags:
                - backend
            parameters:
                - in: path
                  name: backend_id
                  type: integer
            responses:
              201:
                description: backend
            """

