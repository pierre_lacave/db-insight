# -*- coding: utf-8 -*-
"""
    test_login.py
    ~~~~~~~~

    Insight tests
"""

from test import TestCase
import json


class TestLogin(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestLogin, cls).setUpClass()

    def test_signup(self):
        user = {
            'email': 'missing@test.com',
            'password': 'password'
        }
        response = self.client.post("/user", data=json.dumps(user), content_type='application/json')
        data = json.loads(response.data)
        assert len(data) == 2
        assert data['id'] == 1
        assert data['email'] == user['email']
        assert response.status_code == 201

    def test_signup_bad_email(self):
        """
            signup refused because of bad email
        """
        user = {
            'email': 'missing',
            'password': 'password'
        }
        response = self.client.post("/user", data=json.dumps(user), content_type='application/json')
        data = json.loads(response.data)
        expected_error = u'Missing required parameter in the JSON body or the post body or the query string'
        # TODO fix backend to refuse invalid username (not email)
        # assert len(data) == 1
        # assert data['message']['email'] == expected_error
        # assert response.status_code == 400

    def test_signup_missing_email(self):
        """
            signup refused because of lack of email
        """
        user = {
            'password': 'password'
        }
        response = self.client.post("/user", data=json.dumps(user), content_type='application/json')
        data = json.loads(response.data)
        expected_error = u'Missing required parameter in the JSON body or the post body or the query string'
        assert len(data) == 1
        assert data['message']['email'] == expected_error
        assert response.status_code == 400

    def test_signup_missing_password(self):
        """
            signup refused because of lack of password
        """
        user = {
            'email': 'missing@test.com'
        }
        response = self.client.post("/user", data=json.dumps(user), content_type='application/json')
        data = json.loads(response.data)
        expected_error = u'Missing required parameter in the JSON body or the post body or the query string'
        assert len(data) == 1
        assert data['message']['password'] == expected_error
        assert response.status_code == 400

    def test_signup_duplicate(self):
        """
            signup refused because duplicate
        """
        user = {
            'email': 'missing@test.com',
            'password': 'password'
        }
        response = self.client.post("/user", data=json.dumps(user), content_type='application/json')
        data = json.loads(response.data)
        assert response.status_code == 201
        assert len(data) == 2
        assert data['id'] == 1

        response_duplicate = self.client.post("/user", data=json.dumps(user), content_type='application/json')
        data_duplicate = json.loads(response_duplicate.data)
        assert response_duplicate.status_code == 409
        assert len(data_duplicate) == 1
        assert data_duplicate['message'] == u'Email already registered'

    def test_login(self):
        self.addUser("present@test.com", "password")
        user = {
            'username': 'present@test.com',
            'password': 'password'
        }
        response = self.client.post("/auth", data=json.dumps(user), content_type='application/json')
        data = json.loads(response.data)
        assert len(data) == 1
        assert 'token' in data
        assert response.status_code == 200
        response = self.client.get("/user", content_type='application/json', headers={'authorization': 'JWT ' + data['token']})
        data = json.loads(response.data)
        assert data['id'] == 1
        assert data['email'] == user['username']
        assert response.status_code == 200

    def test_login_missing_email(self):
        self.addUser("present@test.com", "password")
        user = {
            'password': 'password'
        }
        response = self.client.post("/auth", data=json.dumps(user), content_type='application/json')
        data = json.loads(response.data)
        assert len(data) == 3
        assert data['status_code'] == 400
        assert data['description'] == u'Missing required credentials'
        assert data['error'] == u'Bad Request'
        assert response.status_code == 400

    def test_login_missing_password(self):
        self.addUser("present@test.com", "password")
        user = {
            'password': 'password'
        }
        response = self.client.post("/auth", data=json.dumps(user), content_type='application/json')
        data = json.loads(response.data)
        assert len(data) == 3
        assert data['status_code'] == 400
        assert data['description'] == u'Missing required credentials'
        assert data['error'] == u'Bad Request'
        assert response.status_code == 400