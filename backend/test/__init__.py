# -*- coding: utf-8 -*-
"""
    __init__.py
    ~~~~~~~~

"""
from flask.ext.testing import TestCase as Base
from app import create_app
from app.config import TestConfig
from app.user.model import User
from app.backend.model import Backend
from app.extensions import db
import logging


class TestCase(Base):
    
    """
    Base TestClass
    """
    @classmethod
    def setUpClass(cls):
        pass

    def create_app(self):
        app = create_app(TestConfig(), app_name="testApp")
        return app

    def setUp(self):
        self.log = logging.getLogger( "insight" )
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def addUser(self, email, password):
        user = User(email, password)
        db.session.add(user)
        db.session.commit()
        return user

    def addBackend(self, name, host, type, user):
        backend = Backend(type, host, name, user.id)
        db.session.add(backend)
        db.session.commit()
        return backend

    def tokenForUser(self, user):
        import flask_jwt
        return flask_jwt.generate_token(user)


