# -*- coding: utf-8 -*-
"""
    test_backend.py
    ~~~~~~~~

    Insight tests
"""

from test import TestCase
from app.backend.model import Backend
import json


class TestBackend(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestBackend, cls).setUpClass()

    def test_backend(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        assert user.id == 1
        backend = {
            'name': 'backend',
            'host': 'postgresql://pierrelacave@localhost/test',
            'type': 'postgres'
        }
        response = self.client.post("/backend",
                                    data=json.dumps(backend),
                                    content_type='application/json',
                                    headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 4
        assert data['id'] == 1
        assert data['host'] == backend['host']
        assert data['name'] == backend['name']
        assert data['type'] == backend['type']
        assert response.status_code == 201
        backend_created = Backend.query.filter_by(id=1).one()
        backend_created.user_id = 1
        pass

    def test_backend_no_name(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        assert user.id == 1
        backend = {
            'host': 'postgresql://pierrelacave@localhost/test',
            'type': 'postgres'
        }
        response = self.client.post("/backend",
                                    data=json.dumps(backend),
                                    content_type='application/json',
                                    headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 1
        assert data['message']['name'] == u'Missing required parameter in the JSON body or the post body or the query string'
        assert response.status_code == 400
        assert Backend.query.count() == 0

    def test_backend_no_url(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        assert user.id == 1
        backend = {
            'name' : "backend",
            'type': 'postgres'
        }
        response = self.client.post("/backend",
                                    data=json.dumps(backend),
                                    content_type='application/json',
                                    headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 1
        assert data['message']['host'] == u'Missing required parameter in the JSON body or the post body or the query string'
        assert response.status_code == 400
        assert Backend.query.count() == 0

    def test_backend_no_type(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        assert user.id == 1
        backend = {
            'name' : "backend",
            'host': 'postgresql://pierrelacave@localhost/test',
        }
        response = self.client.post("/backend",
                                    data=json.dumps(backend),
                                    content_type='application/json',
                                    headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 1
        assert data['message']['type'] == u'Missing required parameter in the JSON body or the post body or the query string'
        assert response.status_code == 400
        assert Backend.query.count() == 0

    def test_backend_bad_url(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        assert user.id == 1
        backend = {
            'name': 'backend',
            'host': 'postgresql://tt@localhost/test',
            'type': 'postgres'
        }
        # TODO catch exception in backend
        # response = self.client.post("/backend",
        #                            data=json.dumps(backend),
        #                            content_type='application/json',
        #                            headers={'authorization': 'JWT ' + token})
        # data = json.loads(response.data)
        # assert len(data) == 4
        # assert data['id'] == 1
        # assert data['host'] == backend['host']
        # assert data['name'] == backend['name']
        # assert data['type'] == backend['type']
        # assert response.status_code == 201
        # backend_created = Backend.query.filter_by(id=1).one()
        # backend_created.user_id = 1

    def test_backend_bad_type(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        assert user.id == 1
        backend = {
            'name': 'backend',
            'host': 'postgresql://pierrelacave@localhost/test',
            'type': 'invalid'
        }
        # TODO catch exception in backend
        #response = self.client.post("/backend",
        #                            data=json.dumps(backend),
        #                            content_type='application/json',
        #                            headers={'authorization': 'JWT ' + token})
        #data = json.loads(response.data)
        # assert len(data) == 4
        # assert data['id'] == 1
        # assert data['host'] == backend['host']
        # assert data['name'] == backend['name']
        # assert data['type'] == backend['type']
        # assert response.status_code == 201
        # backend_created = Backend.query.filter_by(id=1).one()
        # backend_created.user_id = 1

    def test_backend_duplicate_name(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        assert user.id == 1
        backend = {
            'name': 'backend',
            'host': 'postgresql://pierrelacave@localhost/test',
            'type': 'postgres'
        }
        response = self.client.post("/backend",
                                    data=json.dumps(backend),
                                    content_type='application/json',
                                    headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 4
        assert data['id'] == 1
        assert data['host'] == backend['host']
        assert data['name'] == backend['name']
        assert data['type'] == backend['type']
        assert response.status_code == 201
        backend_created = Backend.query.filter_by(id=1).one()
        backend_created.user_id = 1
        response = self.client.post("/backend",
                                    data=json.dumps(backend),
                                    content_type='application/json',
                                    headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        # TODO should not accept duplicate name
        # assert len(data) == 4
        # assert data['id'] == 1
        # assert data['host'] == backend['host']
        # assert data['name'] == backend['name']
        # assert data['type'] == backend['type']
        # assert response.status_code == 201
        # Backend.query.count() == 1

    def test_modify_name_backend(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        backend = self.addBackend("backend", "postgresql://pierrelacave@localhost/test", "postgres", user)
        backend_put = {
            'name': 'backend2',
            'host': 'postgresql://pierrelacave@localhost/test',
            'type': 'postgres'
        }
        response = self.client.put("/backend/%s" % backend.id,
                                    data=json.dumps(backend_put),
                                    content_type='application/json',
                                    headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 4
        assert data['id'] == 1
        assert data['host'] == backend_put['host']
        assert data['name'] == backend_put['name']
        assert data['type'] == backend_put['type']
        assert response.status_code == 201

    def test_modify_host_backend(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        backend = self.addBackend("backend", "postgresql://pierrelacave@localhost/test", "postgres", user)
        backend_put = {
            'name': 'backend',
            'host': 'postgresql://pierrelacave@localhost/test',
            'type': 'postgres'
        }
        response = self.client.put("/backend/%s" % backend.id,
                                    data=json.dumps(backend_put),
                                    content_type='application/json',
                                    headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 4
        assert data['id'] == 1
        assert data['host'] == backend_put['host']
        assert data['name'] == backend_put['name']
        assert data['type'] == backend_put['type']
        assert response.status_code == 201

    def test_modify_type_backend(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        backend = self.addBackend("backend", "postgresql://pierrelacave@localhost/test", "postgres", user)
        backend_put = {
            'name': 'backend2',
            'host': 'postgresql://pierrelacave@localhost/test',
            'type': 'mongodb'
        }
        response = self.client.put("/backend/%s" % backend.id,
                                    data=json.dumps(backend_put),
                                    content_type='application/json',
                                    headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 4
        assert data['id'] == 1
        assert data['host'] == backend_put['host']
        assert data['name'] == backend_put['name']
        assert data['type'] == backend_put['type']
        assert response.status_code == 201

    def test_modify_id_backend(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        backend = self.addBackend("backend", "postgresql://pierrelacave@localhost/test", "postgres", user)
        backend_put = {
            'id': 2,
            'name': 'backend',
            'host': 'postgresql://pierrelacave@localhost/test',
            'type': 'postgres'
        }
        response = self.client.put("/backend/%s" % backend.id,
                                    data=json.dumps(backend_put),
                                    content_type='application/json',
                                    headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 4
        assert data['id'] == 1
        assert data['host'] == backend_put['host']
        assert data['name'] == backend_put['name']
        assert data['type'] == backend_put['type']
        assert response.status_code == 201

    def test_modify_backend_no_name(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        backend = self.addBackend("backend", "postgresql://pierrelacave@localhost/test", "postgres", user)
        backend_put = {
            'host': 'postgresql://pierrelacave@localhost/test',
            'type': 'postgres'
        }
        response = self.client.put("/backend/%s" % backend.id,
                                    data=json.dumps(backend_put),
                                    content_type='application/json',
                                    headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 1
        assert data['message']['name'] == u'Missing required parameter in the JSON body or the post body or the query string'
        assert response.status_code == 400
        assert Backend.query.count() == 1
        assert Backend.query.first().name == "backend"

    def test_modify_backend_no_url(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        backend = self.addBackend("backend", "postgresql://pierrelacave@localhost/test", "postgres", user)
        backend_put = {
            'name': "backend",
            'type': 'postgres'
        }
        response = self.client.put("/backend/%s" % backend.id,
                                    data=json.dumps(backend_put),
                                    content_type='application/json',
                                    headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 1
        assert data['message']['host'] == u'Missing required parameter in the JSON body or the post body or the query string'
        assert response.status_code == 400
        assert Backend.query.count() == 1
        assert Backend.query.first().name == "backend"

    def test_modify_backend_no_type(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        backend = self.addBackend("backend", "postgresql://pierrelacave@localhost/test", "postgres", user)
        backend_put = {
            'name': "backend",
            'host': 'postgresql://pierrelacave@localhost/test',
        }
        response = self.client.put("/backend/%s" % backend.id,
                                    data=json.dumps(backend_put),
                                    content_type='application/json',
                                    headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 1
        assert data['message']['type'] == u'Missing required parameter in the JSON body or the post body or the query string'
        assert response.status_code == 400
        assert Backend.query.count() == 1
        assert Backend.query.first().name == "backend"

    def test_modify_backend_bad_url(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        backend = self.addBackend("backend", "postgresql://pierrelacave@localhost/test", "postgres", user)
        backend_put = {
            'name': "backend",
            'host': 'postgresql://ffff@localhost/test',
            'type': 'postgres',
        }
        # TODO catch exception backend
        # response = self.client.put("/backend/%s" % backend.id,
        #                             data=json.dumps(backend_put),
        #                             content_type='application/json',
        #                             headers={'authorization': 'JWT ' + token})
        # data = json.loads(response.data)
        # assert len(data) == 1
        # assert data['message']['type'] == u'Missing required parameter in the JSON body or the post body or the query string'
        # assert response.status_code == 400
        # assert Backend.query.count() == 1
        # assert Backend.query.first().name == "backend"

    def test_modify_backend_bad_type(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        backend = self.addBackend("backend", "postgresql://pierrelacave@localhost/test", "postgres", user)
        backend_put = {
            'name': "backend",
            'host': 'postgresql://pierrelacave@localhost/test',
            'type': 'sfsfsf',
        }
        # TODO catch exception backend
        # response = self.client.put("/backend/%s" % backend.id,
        #                             data=json.dumps(backend_put),
        #                             content_type='application/json',
        #                             headers={'authorization': 'JWT ' + token})
        # data = json.loads(response.data)
        # assert len(data) == 1
        # assert data['message']['type'] == u'Missing required parameter in the JSON body or the post body or the query string'
        # assert response.status_code == 400
        # assert Backend.query.count() == 1
        # assert Backend.query.first().name == "backend"

    def test_modify_backend_duplicate_name(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        backend = self.addBackend("backend", "postgresql://pierrelacave@localhost/test", "postgres", user)
        backend_two = self.addBackend("backend2", "postgresql://pierrelacave@localhost/test", "postgres", user)
        backend_put = {
            'name': "backend2",
            'host': 'postgresql://ffff@localhost/test',
            'type': 'postgres',
        }
        # TODO should refuse rename
        # response = self.client.put("/backend/%s" % backend.id,
        #                             data=json.dumps(backend_put),
        #                             content_type='application/json',
        #                             headers={'authorization': 'JWT ' + token})
        # data = json.loads(response.data)
        # assert len(data) == 1
        # assert data['message']['type'] == u'Missing required parameter in the JSON body or the post body or the query string'
        # assert response.status_code == 400
        # assert Backend.query.count() == 1
        # assert Backend.query.first().name == "backend"

    def test_delete_backend(self):
        user = self.addUser("present@test.com", "password")
        token = self.tokenForUser(user)
        assert user.id == 1
        backend = {
            'name': 'backend',
            'host': 'postgresql://pierrelacave@localhost/test',
            'type': 'postgres'
        }
        assert Backend.query.count() == 0
        self.client.post("/backend",
                         data=json.dumps(backend),
                         content_type='application/json',
                         headers={'authorization': 'JWT ' + token})
        assert Backend.query.count() == 1
        response = self.client.delete("/backend/%s" % 1,
                                      content_type='application/json',
                                      headers={'authorization': 'JWT ' + token})
        assert response.status_code == 204
        assert Backend.query.count() == 0

    def test_get_two_list_backend(self):
        user = self.addUser("test@test.com", "password")
        token = self.tokenForUser(user)
        backendOne = self.addBackend("backendOne", "postgresql://pierrelacave@localhost/test", "postgres", user)
        backendTwo = self.addBackend("backendTwo", "postgresql://pierrelacave@localhost/test", "postgres", user)
        response = self.client.get("/backend",
                         content_type='application/json',
                         headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 1
        assert type(data['backends'] == list)
        assert len(data['backends']) == 2
        assert data['backends'][0]['id'] == backendOne.id
        assert data['backends'][1]['id'] == backendTwo.id
        assert data['backends'][0]['name'] == backendOne.name
        assert data['backends'][1]['name'] == backendTwo.name
        assert data['backends'][0]['host'] == backendOne.host
        assert data['backends'][1]['host'] == backendTwo.host
        assert data['backends'][0]['type'] == backendOne.btype
        assert data['backends'][1]['type'] == backendTwo.btype

    def test_get_one_list_backend(self):
        user = self.addUser("test@test.com", "password")
        token = self.tokenForUser(user)
        backendOne = self.addBackend("backendOne", "postgresql://pierrelacave@localhost/test", "postgres", user)
        response = self.client.get("/backend",
                         content_type='application/json',
                         headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 1
        assert type(data['backends'] == list)
        assert len(data['backends']) == 1
        assert data['backends'][0]['id'] == backendOne.id
        assert data['backends'][0]['name'] == backendOne.name
        assert data['backends'][0]['host'] == backendOne.host
        assert data['backends'][0]['type'] == backendOne.btype

    def test_get_one_backend(self):
        user = self.addUser("test@test.com", "password")
        token = self.tokenForUser(user)
        backendOne = self.addBackend("backendOne", "postgresql://pierrelacave@localhost/test", "postgres", user)
        response = self.client.get("/backend/%s" % backendOne.id,
                         content_type='application/json',
                         headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 4
        assert data['id'] == backendOne.id
        assert data['host'] == backendOne.host
        assert data['name'] == backendOne.name
        assert data['type'] == backendOne.btype


    def test_get_backend_definition(self):
        user = self.addUser("test@test.com", "password")
        token = self.tokenForUser(user)
        backendOne = self.addBackend("backendOne", "postgresql://pierrelacave@localhost/test", "postgres", user)
        response = self.client.get("/backend/%s/definition" % backendOne.id,
                         content_type='application/json',
                         headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 1
        assert len(data['schema']) > 1
