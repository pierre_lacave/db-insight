# -*- coding: utf-8 -*-
"""
    test_work.py
    ~~~~~~~~

    Insight tests
"""

from test import TestCase
from app.work.model import Work
import json


class TestWork(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TestWork, cls).setUpClass()

    def test_work(self):
        user = self.addUser("test@test.com", "password")
        token = self.tokenForUser(user)
        backend = self.addBackend("backend", "postgresql://pierrelacave@localhost/test", "postgres", user)
        work = {
            'backend': backend.id,
        }
        response = self.client.post("/work",
                                    data=json.dumps(work),
                                    content_type='application/json',
                                    headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)

        assert len(data) == 3
        assert data['id'] == 1
        assert data['backend_id'] == 1

        work_put = {
            'backend': backend.id,
            'config': {
                'measure': 'backend',
                'dimension': 'name'
            }
        }

        response = self.client.put("/work/%s" % data['id'],
                                   data=json.dumps(work_put),
                                   content_type='application/json',
                                   headers={'authorization': 'JWT ' + token})
        data = json.loads(response.data)
        assert len(data) == 3
        assert len(data['config']) == 2

