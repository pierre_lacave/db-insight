# -*- coding: utf-8 -*-
"""
    test_authorisation.py
    ~~~~~~~~

    Insight tests
"""

from test import TestCase
import json


class TestAuthorisation(TestCase):
    def test_all_tested(self):
        """
        Test that we have a test for each URL endpoint, and that all tests are meaningful
        :return:
        """
        import inspect
        funcs = [func[0] for func in inspect.getmembers(self, predicate=inspect.ismethod)]
        urls = []
        for rule in self.app.url_map.iter_rules():
            for method in rule.methods:
                if method not in ['OPTIONS', 'HEAD']:
                    url = 'test_{endpoint}_{method}'.format(method=method, endpoint=rule.endpoint)
                    assert url in funcs
                    urls.append(url)
        for func in funcs:
            if func.startswith('test_') and func != 'test_all_tested':
                assert func in urls

    def test_static_files_GET(self):
        pass

    def test_root_GET(self):
        pass

    def test_spec_GET(self):
        pass

    def test_static_GET(self):
        pass

    def test_jwt_POST(self):
        pass

    def test_userrest_POST(self):
        pass

    def test_userrest_GET(self):
        response = self.client.get("/user", content_type='application/json')
        data = json.loads(response.data)
        expected_error = u'Authorization Required'
        assert len(data) == 3
        assert data['error'] == expected_error
        assert response.status_code == 401

    def test_backendrestlist_POST(self):
        response = self.client.post("/backend", data={}, content_type='application/json')
        data = json.loads(response.data)
        expected_error = u'Authorization Required'
        assert len(data) == 3
        assert data['error'] == expected_error
        assert response.status_code == 401

    def test_backendrestlist_GET(self):
        response = self.client.get("/backend", content_type='application/json')
        data = json.loads(response.data)
        expected_error = u'Authorization Required'
        assert len(data) == 3
        assert data['error'] == expected_error
        assert response.status_code == 401
        pass

    def test_backendrest_PUT(self):
        response = self.client.put("/backend/1", data={}, content_type='application/json')
        data = json.loads(response.data)
        expected_error = u'Authorization Required'
        assert len(data) == 3
        assert data['error'] == expected_error
        assert response.status_code == 401

    def test_backendrest_DELETE(self):
        response = self.client.delete("/backend/1", content_type='application/json')
        data = json.loads(response.data)
        expected_error = u'Authorization Required'
        assert len(data) == 3
        assert data['error'] == expected_error
        assert response.status_code == 401

    def test_backendrest_GET(self):
        response = self.client.get("/backend/1", content_type='application/json')
        data = json.loads(response.data)
        expected_error = u'Authorization Required'
        assert len(data) == 3
        assert data['error'] == expected_error
        assert response.status_code == 401

    def test_backenddefinitionrest_GET(self):
        response = self.client.get("/backend/1/definition", content_type='application/json')
        data = json.loads(response.data)
        expected_error = u'Authorization Required'
        assert len(data) == 3
        assert data['error'] == expected_error
        assert response.status_code == 401

    def test_workrestlist_GET(self):
        response = self.client.get("/work", content_type='application/json')
        data = json.loads(response.data)
        expected_error = u'Authorization Required'
        assert len(data) == 3
        assert data['error'] == expected_error
        assert response.status_code == 401

    def test_workrestlist_POST(self):
        response = self.client.post("/work", data={}, content_type='application/json')
        data = json.loads(response.data)
        expected_error = u'Authorization Required'
        assert len(data) == 3
        assert data['error'] == expected_error
        assert response.status_code == 401

    def test_workrest_PUT(self):
        response = self.client.put("/work/1", data={}, content_type='application/json')
        data = json.loads(response.data)
        expected_error = u'Authorization Required'
        assert len(data) == 3
        assert data['error'] == expected_error
        assert response.status_code == 401

    def test_workrest_DELETE(self):
        response = self.client.delete("/work/1", data={}, content_type='application/json')
        data = json.loads(response.data)
        expected_error = u'Authorization Required'
        assert len(data) == 3
        assert data['error'] == expected_error
        assert response.status_code == 401

    def test_workdatarest_GET(self):
        response = self.client.get("/work/1/data", content_type='application/json')
        data = json.loads(response.data)
        expected_error = u'Authorization Required'
        assert len(data) == 3
        assert data['error'] == expected_error
        assert response.status_code == 401
